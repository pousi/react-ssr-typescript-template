import { Record } from 'immutable';

export interface Reducer<StateType, ActionType> {
  (state?: StateType, action?: ActionType): StateType;
}

export type ReducerMap<PlainType, ActionType> = {
  [P in keyof PlainType]: Reducer<PlainType[P], ActionType>;
};

export function combineReducers<RecordType extends Record<PlainType>, PlainType, ActionType>(
  reducers: ReducerMap<PlainType, ActionType>,
  initialState: RecordType,
): Reducer<RecordType, ActionType> {
  return function(state: RecordType = initialState, action: ActionType): RecordType {
    let newState = state;

    for (const key in reducers) {
      if (reducers.hasOwnProperty(key)) {
        const reducer = reducers[key];
        newState = newState.set(key, reducer(newState.get(key), action));
      }
    }

    return newState;
  };
}
