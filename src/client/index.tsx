import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { fromJS } from 'immutable';
import { AppContainer as ReactHotLoader } from 'react-hot-loader';

import { createStore, StateRecord, Store } from '../store';

import App from '../containers/App';
import ErrorBoundary from '../components/ErrorBoundary';

// Create application store from hydrated state
const initialState: StateRecord = fromJS(window.__STATE__);
const store: Store = createStore(initialState);

const rootElement: HTMLElement = document.getElementById('root');
delete window.__STATE__;

const composeApp = (Component: any) => (
  <ReactHotLoader key={Math.random()}>
    <ErrorBoundary>
      <Component store={store} />
    </ErrorBoundary>
  </ReactHotLoader>
);

ReactDOM.hydrate(composeApp(App), rootElement);

if (module.hot) {
  module.hot.accept(() => ReactDOM.render(composeApp(App), rootElement));
}
