import { Record } from 'immutable';
import { createStore as _createStore, Store as _Store } from 'redux';

import { combineReducers, ReducerMap, Reducer } from './combine-reducers';
import * as tasks from './concepts/tasks';

type Action = tasks.TasksAction;

export interface State extends
  tasks.ApplicationState
  { }

export type StateRecord = Record<State>;

export type Store = _Store<StateRecord>;

export const DEFAULT_INITIAL_STATE: Record<State> = Record<State>({
  tasks: tasks.defaultState,
})();

const reducers: ReducerMap<State, Action> = {
  tasks: tasks.default,
}

export function createStore(
  initialState: StateRecord = DEFAULT_INITIAL_STATE,
): Store {
  const reducer: Reducer<StateRecord, Action> = combineReducers<StateRecord, State, Action>(
    reducers, initialState
  );

  return _createStore(reducer);
}