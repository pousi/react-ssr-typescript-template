import * as React from 'react';

interface IErrorBoundaryProps {
  children: any,
}

interface IState {
  hasError: boolean,
}

class ErrorBoundary extends React.Component<IErrorBoundaryProps, IState> {
  constructor(props : IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error: any, info: any) {
    this.setState({ hasError: true });
    console.log(error, info);
  }

  public render(): JSX.Element {
    const { hasError } = this.state;

    if (hasError) {
      return (<div>error!</div>);
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
