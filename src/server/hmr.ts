import * as Webpack from 'webpack';
import * as devMiddleware from 'webpack-dev-middleware';
import * as hotMiddleware from 'webpack-hot-middleware';
import { Router } from 'express';

const registerHotModuleReload = (app: Router) => {
  const config: Webpack.Configuration = require('../../webpack.client.config.js');
  const compiler: Webpack.Compiler = Webpack(config);

  app.use(devMiddleware(compiler, {
    publicPath: config.output.publicPath,
    noInfo: true,
    lazy: false,
  }));

  app.use(hotMiddleware(compiler));

  return app;
};

export default registerHotModuleReload;
