import * as Promise from 'bluebird';
import * as http from 'http';
import * as express from 'express';
import * as chalk from 'chalk';
import { Express, Request, Response, RequestHandler, NextFunction } from 'express';

import registerHotModuleReload from './hmr'; // tree-shaking
import renderPage from './ssr';

const PORT = 3000;

function asyncMiddleware(handler: RequestHandler): RequestHandler {
  return (req: Request, res: Response, next: NextFunction) =>
    Promise.resolve(handler(req, res, next))
      .catch(next);
};

const app: Express = express();

// Register HMR support
registerHotModuleReload(app);

// Static resources
app.use(express.static('public'));

// Default route returns application html
app.get('*', asyncMiddleware(renderPage));

app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.log('error', err);
  res.status(500).end();
});

// Create server and listen for connections
const server = http.createServer(app);

server.listen(PORT, () => {
  console.log(chalk.green(`\nlistening on port: ${PORT}\n`))
});
