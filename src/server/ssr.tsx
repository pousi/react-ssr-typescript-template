import * as path from 'path';
import * as fse from 'fs-extra';
import * as React from 'react';
import { renderToString } from 'react-dom/server';
import { Request, Response } from 'express';

import { createStore, Store } from '../store';

import App from '../containers/App';

async function loadHtmlTemplate(): Promise<string> {
  return fse.readFile(path.resolve(__dirname, '..', '..', 'dist', 'app.html'), 'utf8');
}

async function renderApp(url: string, res: Response): Promise<void> {
  const store: Store = createStore();
  const template: string = await loadHtmlTemplate();
  const appContent: string = renderToString(<App store={store} />);

  const html: string = template
    .replace('{content}', appContent)
    .replace('{state}', JSON.stringify(store.getState().toJS()));

  res.send(html);
}

async function renderPage(req: Request, res: Response) {
  try {
    return renderApp(req.url, res);
  } catch (error) {
    console.error(error);
  }
}

export default renderPage;
