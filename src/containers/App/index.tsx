import * as React from 'react';
import { Provider } from 'react-redux';

import { Store } from '../../store';
import TaskList from '../TasksList';

import * as styles from './app.scss';

interface AppProps {
  store: Store,
}

const App: React.StatelessComponent<AppProps> = (props): JSX.Element => (
  <Provider store={props.store}>
    <div className={styles.container}>
      <h1 className={styles.title}>Application</h1>
      <TaskList />
    </div>
  </Provider>
);

export default App;
