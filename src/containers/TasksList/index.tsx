import * as React from 'react';
import { connect } from 'react-redux';
import { List } from 'immutable';

import { StateRecord } from '../../store';
import { TaskRecord } from '../../concepts/tasks';

import * as styles from './tasks-list.scss';

interface Props { }

interface StateToProps {
  tasks: List<TaskRecord>,
}

type TaskListProps = Props & StateToProps;

const TasksList: React.StatelessComponent<TaskListProps> = (props): JSX.Element => (
  <ul className={styles.list}>
    {props.tasks.map((task: TaskRecord) => (
      <li key={task.get('title')}>{task.get('title')}</li>
    ))}
  </ul>
);

const mapStateToProps = (state: StateRecord): TaskListProps => ({
  tasks: state.get('tasks'),
});


export default connect<StateToProps>(mapStateToProps)(TasksList);
