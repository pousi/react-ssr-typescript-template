import { Record, List } from 'immutable';

import { OtherAction } from './other';

export type ADD_TASK = 'tasks/ADD_TASK';
export const ADD_TASK: ADD_TASK = 'tasks/ADD_TASK';
export type AddTaskAction = {
  type: ADD_TASK,
  payload: {
    title: string,
  },
};

export function addTask(title: string): AddTaskAction {
  return {
    type: ADD_TASK,
    payload: { title },
  };
}

export interface Task {
  title: string,
  estimate?: number
};

export type TaskRecord = Record<Task>;

export interface TasksState extends List<TaskRecord> { }

export interface ApplicationState {
  tasks: TasksState,
}

const taskFactory = Record<Task>({
  title: '',
  estimate: undefined,
});

export const defaultState = List<TaskRecord>([taskFactory({ title: 'my first task '})]);

export type TasksAction = AddTaskAction | OtherAction;

export default function reducer(
  state: TasksState = defaultState,
  action: TasksAction
) {
  switch (action.type) {
    case ADD_TASK: {
      return state.push(taskFactory(action.payload));
    }

    default: {
      return state;
    }
  }
}