const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const SimpleProgressPlugin = require('webpack-simple-progress-plugin');

const helpers = require('./webpack.helpers');

const NODE_ENV = process.env.NODE_ENV || 'development';

const common = merge(
  {
    target: 'node',
    externals: [nodeExternals()],
    node: {
      __dirname: true,
    },
    entry: [
      'babel-polyfill',
      './src/server/index.ts',
    ],
    output: {
      filename: 'server.js',
      path: path.resolve(__dirname, 'dist'),
      libraryTarget: 'commonjs',
    },
  },
  helpers.setupExtensions(),
  helpers.setupCSS({ target: 'node', env: NODE_ENV }),
  helpers.setupPlugins(),
  helpers.setupTypescript('tsconfig.server.json')
);

let webpackConfig = common;

module.exports = webpackConfig;