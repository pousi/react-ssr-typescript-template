# `react-ssr-typescript-template`

Opionated take on universal React application with bells and whistles template.

## Get started

1. `yarn`
2. `yarn start`
3. `open localhost:3000`

** NOTE: currently you have to run `yarn start` twice because generating typings for styles does not work as I would have wanted. **

## Features

- [x] Typescript
- [x] React with SSR and HMR
- [x] Redux
- [x] redux-thunk
- [x] immutablejs
- [ ] react-router-v4
- [ ] Jest
- [ ] TSLint
