const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const SimpleProgressPlugin = require('webpack-simple-progress-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV | 'development';

module.exports = {}

module.exports.setupExtensions = function setupExtensions() {
  return {
    resolve: {
      modules: [path.join(__dirname, 'src'), 'node_modules'],
      extensions: ['.ts', '.tsx', '.js', '.jsx', '.json', '.scss'],
    },
  }
}

module.exports.setupTypescript = function setupTypescript(configFile) {
  return {
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                configFile,
              }
            }
          ]
        },          
      ]
    }
  }
}

module.exports.setupCSS = function setupCSS(opts) {
  const _opts = Object.assign({ target: 'browser', env: 'development' }, opts);

  const loaders = [
    {
      loader: 'typings-for-css-modules-loader',
      options: {
        modules: true,
        namedExport: true,
        camelCase: true,
        importLoaders: 2,
        localIdentName: '[name]__[local]__[hash:5]',
      },
    },
    { loader: 'postcss-loader' },
    { loader: 'sass-loader' }
  ];

  if (_opts.target === 'node') {
    loaders.unshift({ loader: 'isomorphic-style-loader' });
  }

  if (_opts.env === 'development' && opts.target === 'browser') {
    loaders.unshift({ loader: 'style-loader' });
  }

  return {
    module: {
      rules: [
        {
          test: /\.scss$/,
          exclude: /node_modules/,
          enforce: 'pre',
          use: _opts.env === 'production'
            ? ExtractTextPlugin.extract({ fallback: 'style-loader', use: loaders })
            : loaders
        },
      ],
    },
  };
}

module.exports.setupPlugins = function setupPlugins() {
  return {
    plugins: [
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify(NODE_ENV) || 'development',
      }),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
    ]
  }
}