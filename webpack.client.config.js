const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const helpers = require('./webpack.helpers.js');

const NODE_ENV = process.env.NODE_ENV || 'development';

const common = merge(
  {
    entry: [
      'babel-polyfill',
      'react-hot-loader/patch',
      'webpack-hot-middleware/client',
      './src/client/index.tsx',
    ],
    output: {
      filename: 'bundle.js',
      chunkFilename: '[name].[chunkhash].js',
      path: path.resolve(__dirname, 'dist', 'public'),
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        template: path.join('src', 'app.tmpl.html'),
        title: 'Universal template',
        filename: 'app.html',
        inject: 'body',
        alwaysWriteToDisk: true,
      }),
      new HtmlWebpackHarddiskPlugin({
        outputPath: path.resolve(__dirname || '.', 'dist'),
      }),
    ],   
  },
  helpers.setupExtensions(),
  helpers.setupCSS({ target: 'browser', env: NODE_ENV }),
  helpers.setupPlugins(),
  helpers.setupTypescript('tsconfig.json')
);

let webpackConfig = common;

module.exports = webpackConfig;